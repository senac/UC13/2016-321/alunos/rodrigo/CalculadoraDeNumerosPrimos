/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradenumerosprimos;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Diamond
 */
public class CalculadoraDePrimoTest {
    
    private CalculadoraDePrimos calculadora;    
    
    private int[] numeros;    
    
    @Before
    public void init() {
        calculadora = new CalculadoraDePrimos();        
        numeros = new int[]{
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53,
            59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113,
            127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191,
            193, 197, 199, 211, 223, 227, 229, 233, 239, 241, 251, 257, 263,
            269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347,
            349, 353, 359, 367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431,
            433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509,
            521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613,
            617, 619, 631, 641, 643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709,
            719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787, 797, 809, 811, 821,
            823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919,
            929, 937, 941, 947, 953, 967, 971, 977, 983, 991, 997 
        } ; 
        
    }
    
    @Test
    public void NumeroNaoDeveSerPrimo() {
        assertEquals(false, calculadora.isPrimo(1));
    }
    
    @Test
    public void deveApenasTerDuasDivisoesParaNumero2() {
        assertEquals(true, calculadora.isPrimo(2));
    }
    
    @Test
    public void deveApenasTerDuasDivisoesParaNumero997() {
        assertEquals(true, calculadora.isPrimo(997));
    }
    
    @Test
    public void deveApenasPossuirNumerosPrimosNoArray() {
        
        for (int numero : numeros) {
            assertEquals(true, calculadora.isPrimo(numero));
        }
        
    }
    
    
    @Test
    public void naoDeveSerPrimo(){
        assertEquals(false, calculadora.isPrimo(10));
    }
    
    
    
    
    
}

