/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.rodrigo.calculadoradenumerosprimos;

/**
 *
 * @author Diamond
 */
public class CalculadoraDePrimos {
    
    public boolean isPrimo(int numero){
        
        int divisoes = 0 ; 
        
        for(int i = 1 ; i <= numero; i++){
        
            if(numero%i == 0 ){
                divisoes++;
                if(divisoes > 2 ){
                    break;
                }
            }
        }
        
        return divisoes == 2  ;
    }
    
}

    

